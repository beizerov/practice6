package ua.nure.beizerov.practice6;


import ua.nure.beizerov.practice6.part1.Part1;
import ua.nure.beizerov.practice6.part2.Part2;
import ua.nure.beizerov.practice6.part3.Part3;
import ua.nure.beizerov.practice6.part4.Part4;
import ua.nure.beizerov.practice6.part5.Part5;
import ua.nure.beizerov.practice6.part6.Part6;


/**
 * This class is represent a demo.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Demo {

	public static void main(String[] args) {
		System.out.println("=========================== PART1");
		Part1.main(null);
		
		System.out.println("=========================== PART2");
		Part2.main(null);
		
		System.out.println("=========================== PART3");
		Part3.main(null);
		
		System.out.println("=========================== PART4");
		Part4.main(null);
		
		System.out.println("=========================== PART5");
		Part5.main(null);
		
		System.out.println("=========================== PART6 1");
		Part6.main(new String[] {
									"--input", "part6.txt", 
									"--task", "frequency"
								}
		);
		System.out.println("=========================== PART6 2");
		Part6.main(new String[] {
									"--input", "part6.txt",
									"--task", "length"
								}
		);
		System.out.println("=========================== PART6 3");
		Part6.main(new String[] {
									"--input", "part6.txt",
									"--task", "duplicates"
								}
		);
	}
}
