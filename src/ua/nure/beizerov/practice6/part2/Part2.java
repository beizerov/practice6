package ua.nure.beizerov.practice6.part2;


import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * This class is represent a Part2.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part2 {
	
	public static void main(String[] args) {
		int i = 0;
		int n = 17;
		int k = 1;
		
		List<Integer> aListIndex = IntStream
							.rangeClosed(i, n)
							.limit(n)
							.boxed()
							.collect(
								Collectors.toCollection(ArrayList::new)
							)
		;
		
		List<Integer> aListIterator = new ArrayList<>(aListIndex);
		List<Integer> lListIndex = new LinkedList<>(aListIndex);
		List<Integer> lListIterator = new LinkedList<>(aListIndex);
		
		
		System.out.println(
			"ArrayList#Index: " + removeByIndex(aListIndex, k) + " ms"
		);
		System.out.println(
			"LinkedList#Index: " + removeByIndex(lListIndex, k) + " ms"
		);
		System.out.println(
			"ArrayList#Iterator: " + removeByIterator(aListIterator, k) + " ms"
		);
		System.out.println(
			"LinkedList#Iterator: " + removeByIterator(lListIterator, k) + " ms"
		);
	}
	
	
	public static long removeByIndex(List<Integer> list, int k) {
		Instant start = Instant.now();
		int currentPosition = 0;
		k--;
	
		while (list.size() != 1) {
			currentPosition += k;
			if (list.size() - currentPosition > 0) {
				list.remove(currentPosition);
			} else if (list.size() <= currentPosition) {
				currentPosition %= list.size();
				list.remove(currentPosition);
			}
		}
		Instant finish = Instant.now();
		
		return Duration.between(start, finish).toMillis();
	}
	
	public static long removeByIterator(List<Integer> list, int k) {
		Instant start = Instant.now();
		Iterator<Integer> iterator = list.iterator();
		
		int i = 0;
		while (list.size() != 1) {
			i = (i == 0) ? k : i;
			while (iterator.hasNext() && i != 0 && list.size() != 1) {
				iterator.next();
				i--;
				if (i == 0) {
					iterator.remove();
					i = k;
				}
			}
			
			iterator = list.iterator();
		}
		
		Instant finish = Instant.now();
		
		return Duration.between(start, finish).toMillis();
	}
}
