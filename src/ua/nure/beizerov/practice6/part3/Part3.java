package ua.nure.beizerov.practice6.part3;

/**
 * This class is represent a Part3.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part3 {

	public static void main(String[] args) {
		Parking parking = new Parking(5);
		
		// true, 0010
		System.out.printf("%b, ", parking.arrive(2));
		parking.print();
		
		// true, 0011
		System.out.printf("%n%b, ", parking.arrive(3));
		parking.print();

		// true, 1011
		System.out.printf("%n%b, ", parking.arrive(2));
		parking.print();
		
		// true, 1111
		System.out.printf("%n%b, ", parking.arrive(2));
		parking.print();
		
		// false, 1111
		System.out.printf("%n%b, ", parking.arrive(2));
		parking.print();

		// true, 1011
		System.out.printf("%n%b, ", parking.depart(1));
		parking.print();
		
		// false, 1011
		System.out.printf("%n%b, ", parking.depart(1));
		parking.print();
		System.out.println();
		
		
	}
}
