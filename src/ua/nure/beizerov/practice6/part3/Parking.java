package ua.nure.beizerov.practice6.part3;


/**
 * This class is represent a parking.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Parking {

	private static final int FREE_SPOT = 0;
	private static final int SPOT_BUSY = 1;
	
	private int[] spots;

	
	/**
	 * @param n	number of spots.
	 */
	public Parking(int n) {
		this.spots = new int[n];
	}
	

	private boolean takeSpot(int spotNumber) {
		if (spots[spotNumber] == FREE_SPOT) {
			spots[spotNumber] = SPOT_BUSY;
			
			return true;
		}
		
		return false;
	}
	
	public boolean arrive(int k) {
		if (k < 0 || k > spots.length - 1) {
			throw new IllegalArgumentException();
		}
		
		if (takeSpot(k)) {
			return true;
		}
		
		int currentPosition = k + 1;
		while (currentPosition != k) {
			if (currentPosition == spots.length - 1) {
				if (takeSpot(currentPosition)) {
					return true;
				}
				
				currentPosition = 0;
			} else {
				currentPosition++;
			}
			
			if (takeSpot(currentPosition)) {
				return true;
			}
		}

		return false;
	}
	
	public boolean depart(int k) {
		if (k < 0 || k > spots.length - 1) {
			throw new IllegalArgumentException();
		}
		
		if (spots[k] == 1) {
			spots[k] = 0;
			
			return true;
		}
		
		return false;
	}
	
	public void print() {
		for (int i : spots) {
			System.out.print(i);
		}
		System.out.println();
	}
}
