package ua.nure.beizerov.practice6.part4;


import java.util.Iterator;
import java.util.stream.IntStream;


/**
 * This class is represent a range.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Range implements Iterable<Integer> {
	
	private int[] integers;
	
	public Range(int n, int m) {
		this(n, m, false);
	}
	
	public Range(int n, int m, boolean reverse) {
		this.integers = new int[m + 1 - n];
		
		if (reverse) {
			for (int i = 0; i < integers.length; i++) {
				integers[i] = -(i - m);
			}
		} else {
			for (int i = 0; i < integers.length; i++) {
				integers[i] = i + n;
			}
		}
	}
	
	
	@Override
	public Iterator<Integer> iterator() {
		return IntStream
				.of(integers)
				.iterator()
		;
	}
}
