package ua.nure.beizerov.practice6.part5;


/**
 * This class is represent a BST.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Tree<E extends Comparable<E>> {
	
	private Node<E> root;
	private Node<E> reference;
	private int depth = -1;
	
	public boolean add(E element) {
		if (root == null) {
			reference = root = new Node<>(element);
			return true;
		} else {
			reference = root;
		}
		
		Node<E> node = new Node<>(element);
		
		while (reference != null) {
			if (reference.data.compareTo(element) > 0) {
				
				if (reference.right == null) {
					reference.right = node;
					reference = root;
					return true;
				} else {
					reference = reference.right;
				}
				
			} else if (reference.data.compareTo(element) < 0) {
				if (reference.left == null) {
					reference.left = node;
					reference = root;
					return true;
				} else {
					reference = reference.left;
				}
			} else if (reference.data.compareTo(element) == 0) {
				return false;
			}
		}
		
		return false;
	}
	
	public void add(E[] elements) {
		for (E element : elements) {
			this.add(element);
		}
	}
	
	public boolean remove(E element) {
		reference = root;
	
		// References to the previous node, the left and right.
		Node<E> leftNode = null; 
		Node<E> rightNode = null;
		
		while (reference != null) {
			switch (getDirection(reference.data.compareTo(element))) {
				case "HERE":
					if (reference.left == null && reference.right == null) {
						removeNodeWithoutChildren(leftNode, rightNode);
						
						return true;
					} else if (
							reference.left == null && reference.right != null
					) {
						leftNode.left = reference.right;
						
						return true;
					} else if (
							reference.left != null && reference.right == null
					) {
						leftNode.left = reference.left;
						
						return true;
					} else {
						removeNodeWithTwoChildren(leftNode, rightNode);
						
						return true;
					}
				case "RIGHT":
					rightNode = reference;
					reference = reference.right;
					
					continue;
				case "LEFT":
					leftNode = reference;
					reference = reference.left;
					
					continue;
				default:
					break;
			}
		}
		
		return false;
	}
	
	private static String getDirection(int direction) {
		 if (direction < 0) {
			 return "LEFT";
		 } else if (direction > 0) {
			 return "RIGHT";
		 } else {
			 return "HERE";
		 }
	}
	
	private void removeNodeWithoutChildren(Node<E> left, Node<E> right) {
		if (isPreviousLeft(left)) {
			left.left = null;
		} else {
			right.right = null;
		}
	}
	
	private void removeNodeWithTwoChildren(Node<E> left, Node<E> right) {
		if (isPreviousLeft(left)) {
			left.left = reference.left;
			left
				.left
				.right = getNodeWithoutRightChild(reference);
		} else {
			right
				.right = getNodeWithoutRightChild(reference.left);
			reference.left.right = null;
			right.right.right = reference.right;
			right.right.left = reference.left;
		}
	}
	
	private boolean isPreviousLeft(Node<E> ref) {
		return (ref != null && ref.left == reference) ? true : false;
	}
	
	private Node<E> getNodeWithoutRightChild(Node<E> ref) {
		Node<E> node = ref.right;
		
		while (ref.right != null) {
			node = ref.right;
			ref = ref.right;
		}
		
		return node;
	}
	
	public void print() {		
		printNodes(root);
	}
	
	private void printNodes(Node<E> node) {
		// exit from recursion
		if (node == null) {
			return;
		}
		
		depth++;
		printNodes(node.right);
		System.out.printf("%s%s%n",getDepth(depth), node);
		printNodes(node.left);
		depth--;
	}
	
	private static String getDepth(int depth) {
		StringBuilder deep = new StringBuilder();
		
		while (depth-- != 0) {
			deep.append("  ");
		}
		
		return deep.toString();
	}
	
	private static final class Node<E extends Comparable<E>> {
		
		private E data;
		private Node<E> left;
		private Node<E> right;
		
		
		Node(E data) {
			this.data = data;
			this.left = null;
			this.right = null;
		}
		
		
		@Override
		public String toString() {
			return  data.toString();
		}	
	}
}
