package ua.nure.beizerov.practice6.part5;


/**
 * This class is represent a Part5.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part5 {

	public static void main(String[] args) {
		Tree<Integer> tree = new Tree<>();
		
		tree.add(3);
		tree.add(1);
		tree.add(2);
		tree.add(0);
		tree.add(5);
		tree.add(4);
		System.out.println(tree.add(6));
		System.out.println(tree.add(6));
		System.out.println("~~~~~~~");
		tree.print();
		System.out.println("~~~~~~~");
		System.out.println(tree.remove(5));
		System.out.println(tree.remove(5));
		System.out.println("~~~~~~~");
		tree.print();
	}
}
