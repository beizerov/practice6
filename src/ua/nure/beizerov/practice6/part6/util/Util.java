package ua.nure.beizerov.practice6.part6.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public interface Util {
	
	/**
	 * @return a string representation of the file.
	 */
	static String readFile(String path, String encoding) {
        StringBuilder fileResource = new StringBuilder();
        
        try (BufferedReader reader = Files.newBufferedReader(
        		Paths.get(path), Charset.forName(encoding)
        	)) {
        	
        	String line = null;
            while ((line = reader.readLine()) != null) {
                fileResource
                	.append(line)
                	.append(System.lineSeparator());
            }
        } catch (IOException e) {
			e.printStackTrace();
		}
        
        return fileResource.toString();
    }
	
	/**
	 * Remove duplicates without breaking the word order
	 * 
	 * @param strings list of words
	 */
	static void removeDuplicates(List<String> strings) {
		List<String> words = new LinkedList<>(strings);
		int i = 0;
		String string;
		for (String w : words) {
			for (
				Iterator<String> iterator = strings.iterator();
				iterator.hasNext()
				;
			) {
				string = iterator.next();
				if (w.equals(string)) {
					i++;
					if (i > 1) {
						iterator.remove();
					}
				}
				
			}
			i = 0;
		}
	}
}
