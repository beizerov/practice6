package ua.nure.beizerov.practice6.part6;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ua.nure.beizerov.practice6.part6.util.Util;


/**
 * This class is represent a Part62.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part62 {

	public static void main(String[] args) {
		if (args.length != 2) {
			return;
		}
		
		if (!Part6.isNotNullArgs(args)) {
			return;
		}
		
		String input = args[0];
		
		List<String> wordsList = Stream.of(input.split("\\s+"))
				.collect(Collectors.toCollection(LinkedList::new));

		Util.removeDuplicates(wordsList);
		
		String[] words = wordsList.toArray(new String[wordsList.size()]);
		
		Arrays.sort(words, (String w1, String w2) ->
			((Integer) w2.length()).compareTo(w1.length())
		);

		int numberOfWords = Integer.parseInt(args[1]);
		for (int j = 0; j < numberOfWords; j++) {
			System.out.println(
				words[j] + " ==> " + words[j].length()
			);
		}
	}
}
