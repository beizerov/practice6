package ua.nure.beizerov.practice6.part6;

import static ua.nure.beizerov.practice6.part6.util.Util.readFile;


/**
 * This class is represent a Part6.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part6 {

	public static void main(String[] args) {
		if (!validation(args)) {
			return;
		}

		String input = readFile(
			"--input".equals(args[0]) 
			|| 
			"-i".equals(args[0]) 
			? args[1] : args[3]
			
			, "Cp1251"
		);
	
		String command = isCommand(args[1]) ? args[1] : args[3];
		switch (command) {
			case "frequency":
				Part61.main(new String[] {input, "3"});
				
				break;
			case "length":
				Part62.main(new String[] {input, "3"});
				
				break;
			case "duplicates":
				Part63.main(new String[] {input, "3"});
				
				break;
			default:
				break;
		}
	}
	
	private static boolean isCommand(String argument) {
		return (
				"frequency".equals(argument) 
				|| 
				"length".equals(argument) 
				|| 
				"duplicates".equals(argument)
		) ? true : false;
	}
	
	
	/**
	 * @param args string array.
	 * @return true if everything is fine with args.
	 */
	private static boolean validation(String[] args) {
		if(args.length != 4) {
			return false;
		}
		
		if (!isNotNullArgs(args)) {
			return false;
		}
		
		return checkFlags(args);
	}
	
	private static boolean checkFlags(String[] args) {
		boolean flag1 = (
				("--input".equals(args[0]) 
				|| 
				"-i".equals(args[0]))
				&& 
				!args[0].equals(args[2])
		) ? true : false;
		boolean flag2 = (
				"--task".equals(args[2]) 
				|| 
				"-t".equals(args[2])
		) ? true : false;
		
		return flag1 || flag2;
	}
	
	public static boolean isNotNullArgs(String[] args) {
		for (String string : args) {
			if (string == null) {
				return false;
			}
		}
		
		return true;
	}
}
