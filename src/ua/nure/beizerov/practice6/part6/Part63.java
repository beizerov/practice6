package ua.nure.beizerov.practice6.part6;

import java.util.Iterator;
import java.util.Locale;

import ua.nure.beizerov.practice6.part1.Word;
import ua.nure.beizerov.practice6.part1.WordContainer;

/**
 * This class is represent a Part63.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part63 {

	public static void main(String[] args) {
		if (args.length != 2) {
			return;
		}
		
		if (!Part6.isNotNullArgs(args)) {
			return;
		}
		
		String input = args[0];
		int numberOfWords = Integer.parseInt(args[1]);
		
		WordContainer wordContainer = WordContainer.createWordContainer(
				input.replaceAll("\\b[^\\p{IsLatin}]+\\b", " ")
		);
		
		String[] allWords = input.split("\\s+");
		String[] words = new String[numberOfWords];
		
		int i = 0; 
		Word word;
		for (String string : allWords) {
			for (Iterator<Word> iterator = wordContainer.iterator()
				; iterator.hasNext()
				;
			) {
				word = iterator.next();
				if (
					string.equals(word.getContent()) 
					&& word.getFrequency() > 1
					&& i < 3
				) {
					words[i++] = word.getContent().toUpperCase(Locale.ENGLISH);
				}
			}
		}
		
		for (String string : words) {
			System.out.println(new StringBuilder(string).reverse());
		}
	}
}
