package ua.nure.beizerov.practice6.part6;


import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ua.nure.beizerov.practice6.part1.Word;
import ua.nure.beizerov.practice6.part1.WordContainer;
import ua.nure.beizerov.practice6.part6.util.Util;

/**
 * This class is represent a Part61.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part61 {

	public static void main(String[] args) {
		if (args.length != 2) {
			return;
		}
		
		if (!Part6.isNotNullArgs(args)) {
			return;
		}
		
		String input = args[0];
		int numberOfWords = Integer.parseInt(args[1]);
		
		WordContainer wordContainer = WordContainer.createWordContainer(input);
		
		
		List<String> wordsList = Stream.of(input.split("\\s+"))
				.collect(Collectors.toCollection(LinkedList::new));

		Util.removeDuplicates(wordsList);
		
		int frequency = wordContainer.getMaxFrequency();
		
		Word[] words = new Word[numberOfWords];
		
		int i = 0;
		Word word;
		Iterator<Word> iterator = wordContainer.iterator();
		while (i != numberOfWords) {
			for (String string : wordsList) {
				while (iterator.hasNext() && i < numberOfWords) {
				word = iterator.next();
					if (
						string.equals(word.getContent()) 
						&& 
						word.getFrequency() == frequency
					) {
						words[i++] = word;
					}
				}
				
				iterator = wordContainer.iterator();
			}
			
			frequency--;
		}
		
		Arrays.sort(words, (Word w1, Word w2) -> 
			w2.getContent().compareTo(w1.getContent())
		);
		
		for (Word w : words) {
			System.out.println(w.toString().replace(":", "==>"));
		}
	}
}
