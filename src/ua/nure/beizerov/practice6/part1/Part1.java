package ua.nure.beizerov.practice6.part1;


import java.io.ByteArrayInputStream;
import java.io.InputStream;


/**
 * This class is represent a Part1.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Part1 {

	public static void main(String[] args) {
		InputStream in = System.in;
		// set the mock input
		System.setIn(
			new ByteArrayInputStream(
				("asd 43 asdf asd 43\n" + 
				"434 asdf\n" + 
				"kasdf asdf stop asdf\n" + 
				"stop").getBytes()
			)
		);
		
		WordContainer.main(null);
		
		// restore the standard input
		System.setIn(in);
	}
}
