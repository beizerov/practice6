package ua.nure.beizerov.practice6.part1;


/**
 * This class is represent a word.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Word implements Comparable<Word> {

	private final String content;
	private int frequency = 1;
	
	
	/**
	 * @param content
	 */
	public Word(String content) {
		this.content = content;
	}

		
	public String getContent() {
		return content;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Word other = (Word) obj;
		
		if (content == null) {
			if (other.content != null) {
				return false;
			}
		} else if (!content.equals(other.content)) {
			return false;
		}
		
		return true;
	}

	
	@Override
	public int compareTo(Word word) {
		if (this.frequency > word.frequency) {
			return -1;
		} else if (this.frequency < word.frequency) {
			return 1;
		} else {
			return this.content.compareTo(word.content);
		}
	}
	

	@Override
	public String toString() {
		return content + " : " + frequency;
	}
}
