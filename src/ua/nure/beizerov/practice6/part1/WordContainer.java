package ua.nure.beizerov.practice6.part1;


import java.util.Set;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Scanner;


/**
 * This class is represent a word container.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public final class WordContainer implements Iterable<Word> {

	private Set<Word> words;
	

	private WordContainer() {
		
	}


	public static void main(String[] args) {
		try (Scanner scanner =  new Scanner(System.in)) {
			StringBuilder text = new StringBuilder();
			
			String word;
			while (scanner.hasNext()) {
				word = scanner.next();
				
				if ("stop".equals(word)) {
					break;
				}
				
				text
					.append(word)
					.append(' ')
				;
			}
			
			WordContainer container = WordContainer.createWordContainer(
				text.toString()
			); 
			
			container.forEach(System.out::println);
		}
	}

	
	public static WordContainer createWordContainer(String text) {
		WordContainer wordContainer = new WordContainer();
		String[] tokens = text.split("\\s+");
		
		wordContainer.words = new HashSet<>(
			Stream.of(tokens)
				.map(Word::new)
				.collect(Collectors.toSet())
		);

		int i = 0;
		for (Word word : wordContainer.words) {
			for (String token : tokens) {
				if (word.getContent().equals(token)) {
					i++;
				}
			}
			
			word.setFrequency(i);
			i = 0;
		}
		
		wordContainer.words = new TreeSet<>(wordContainer.words);
		
		return wordContainer;
	}

	public int getMaxFrequency() {
		return (words.iterator().hasNext()) 
				? words.iterator().next().getFrequency() 
				: -1;
	}

	public int size() {
		return (words != null) ? words.size() : -1;
	}

	
	@Override
	public Iterator<Word> iterator() {
		return words.iterator();
	}

	@Override
	public String toString() {
		return words.toString();
	}
}
